import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';

import App from './App';

// так выглядит дерево состояния всего приложения
const initialState = {
  searchString: "",
  onlyShowProductsInStock: false,
  newProduct: {
    category: "",
    price: "$0",
    stocked: true,
    name: ""
  },
  products: [
    {category: "Sporting Goods", price: "$49.99", stocked: true, name: "Football"},
    {category: "Sporting Goods", price: "$9.99", stocked: true, name: "Baseball"},
    {category: "Sporting Goods", price: "$29.99", stocked: false, name: "Basketball"},
    {category: "Electronics", price: "$99.99", stocked: true, name: "iPod Touch"},
    {category: "Electronics", price: "$399.99", stocked: false, name: "iPhone 5"},
    {category: "Electronics", price: "$199.99", stocked: true, name: "Nexus 7"}
  ]
};

// оно никак не используется в приложении и нужно тут чисто для справки
const changeSearchStringAction = {
  type: "CHANGE_SEARCH_STRING",
  value: ""
};

// оно никак не используется в приложении и нужно тут чисто для справки
const toggleOnlyShowProductsInStockAction = {
  type: "TOGGLE_ONLY_SHOW_PRODUCTS_IN_STOCK",
  value: false
};

// оно никак не используется в приложении и нужно тут чисто для справки
const removeProductAction = {
  type: "REMOVE_PRODUCT",
  productName: ""
};

// оно никак не используется в приложении и нужно тут чисто для справки
const changeNewProductCategoryAction = {
  type: "CHANGE_NEW_PRODUCT_CATEGORY",
  newProductCategory: ""
};

// оно никак не используется в приложении и нужно тут чисто для справки
const changeNewProductPriceAction = {
  type: "CHANGE_NEW_PRODUCT_PRICE",
  newProductPrice: "$0"
};

// оно никак не используется в приложении и нужно тут чисто для справки
const changeNewProductStockedAction = {
  type: "CHANGE_NEW_PRODUCT_STOCKED",
  newProductStocked: true
};

// оно никак не используется в приложении и нужно тут чисто для справки
const changeNewProductNameAction = {
  type: "CHANGE_NEW_PRODUCT_NAME",
  newProductName: ""
};

// оно никак не используется в приложении и нужно тут чисто для справки
const addProduct = {
  type: "ADD_PRODUCT", // TODO reduce
  category: "",
  price: "$0",
  stocked: true,
  name: ""
};

const productSorter = (product1, product2) => {
  if (product1.category < product2.category) {
    return -1;
  }

  if (product1.category > product2.category)  {
    return 1;
  }

  if (product1.name < product2.name) {
    return -1;
  }

  if (product1.name > product2.name) {
    return 1;
  }

  return 0;
};

// пока что обрабатываем все действия одним редьюсером
function rootReducer(state, action) {
  switch (action.type) {
    case changeSearchStringAction.type:
      return {
        ...state,
        searchString: action.value
      };
    case toggleOnlyShowProductsInStockAction.type:
      return {
        ...state,
        onlyShowProductsInStock: !state.onlyShowProductsInStock
      };
    case removeProductAction.type:
      return {
        ...state,
        products: state.products.filter(product =>
          product.name !== action.productName)
      };
    case  changeNewProductCategoryAction.type:
      return {
        ...state,
        newProduct: {
          ...state.newProduct,
          category: action.newProductCategory
        }
      };
    case changeNewProductPriceAction.type:
      return {
        ...state,
        newProduct: {
          ...state.newProduct,
          price: action.newProductPrice
        }
      };
    case changeNewProductStockedAction.type:
      return {
        ...state,
        newProduct: {
          ...state.newProduct,
          stocked: action.newProductStocked
        }
      };
    case changeNewProductNameAction.type:
      return {
        ...state,
        newProduct: {
          ...state.newProduct,
          name: action.newProductName
        }
      };
    case addProduct.type:
      return {
        ...state,
        products: state.products
          .concat({ ...state.newProduct })
          .sort(productSorter)
      };
    default:
      return state;
  }
}

const store = createStore(rootReducer, initialState);

const render = () => ReactDOM.render(<App store={store}/>, document.getElementById('root'));

store.subscribe(render);

render();
