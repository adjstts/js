import React from 'react';

import Category from './Category';
import Product from './Product';

const Products = props => {
  const state = props.store.getState();

  const searchStringFilter = product => {
    const searchStringInLowerCase = state.searchString.toLowerCase();

    return product.category.toLowerCase().includes(searchStringInLowerCase)
      || product.name.toLowerCase().includes(searchStringInLowerCase);
  };

  const inStockOnlyFilter = product => 
    !state.onlyShowProductsInStock || product.stocked === true;

  const productAndCategoryReducer = () => {
    let lastCategory = null;

    return (products, product) => {
      let toBeAdded = [];

      const category = product.category;
      if (category !== lastCategory) {
        toBeAdded.push(<Category key={category} name={category}/>);
        lastCategory = category;
      }

      toBeAdded.push(
        <Product store={props.store}
          key={product.name}
          {...product}/>);

      return products.concat(toBeAdded);
    };
  };

  const filteredProductAndCategoryRows = state.products
    .filter(searchStringFilter)
    .filter(inStockOnlyFilter)
    .reduce(productAndCategoryReducer(), []);

  return (
    <table>
      <tr>
        <th>Name</th>
        <th>Price</th>
        <th>Actions</th>
      </tr>
      { filteredProductAndCategoryRows }
    </table>
  );
};

export default Products;
