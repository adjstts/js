import React from 'react';

import AddProduct from './AddProduct';
import Products from './Products';

import { changeSearchString, toggleOnlyShowProductsInStock } from './actions';

const App = (props) => {
  return (
    <div style={{width: "30%"}}>

      <h2>Add new product</h2>
      <AddProduct store={props.store}/>

      <h2>Product list</h2>
      <input type="search"
        value={props.store.getState().searchString}
        onChange={event =>
          changeSearchString(event.target.value)}
        placeholder="Search..."/>
      <p>
        <input type="checkbox"
          checked={props.store.getState().onlyShowProductsInStock}
          onChange={event =>
            toggleOnlyShowProductsInStock(event.target.checked)}/>
          Only show products in stock
      </p>
      <Products store={props.store}/>
    </div>
  );
};

export default App;
