import React from 'react';

const Category = ({name}) => 
  <tr>
    <td colSpan="2"><b>{name}</b></td>
  </tr>;

export default Category;
