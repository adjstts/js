import React from 'react';

const ActionLink = ({text, onClick}) => 
  <a href="/#" onClick={onClick}>{text}</a>;

export default ActionLink;
