import React from 'react';

const AddProduct = props => {
  const store = props.store;
  const state = store.getState();

  const changeNewProductCategory = event =>
    store.dispatch({
      type: "CHANGE_NEW_PRODUCT_CATEGORY",
      newProductCategory: event.target.value
    });

  const changeNewProductPrice = event =>
    store.dispatch({
      type: "CHANGE_NEW_PRODUCT_PRICE",
      newProductPrice: event.target.value
    });

  const changeNewProductStocked = event =>
    store.dispatch({
      type: "CHANGE_NEW_PRODUCT_STOCKED",
      newProuctStocked: event.target.checked
    });

  const changeNewProductName = event =>
    store.dispatch({
      type: "CHANGE_NEW_PRODUCT_NAME",
      newProductName: event.target.value
    });

  const newProduct = state.newProduct;

  const addProduct = event => {
    event.preventDefault();

    store.dispatch({
      type: "ADD_PRODUCT",
      ...newProduct
    });
  };

  return (
    <form onSubmit={addProduct}>
      <label>
        Category:
        <input type="text" value={newProduct.category} onChange={changeNewProductCategory}/>
      </label>
      <br/>
      <label>
        Price:
        <input type="text" value={newProduct.price} onChange={changeNewProductPrice}/>
      </label>
      <br/>
      <label>
        Stocked:
        <input type="checkbox" checked={newProduct.stocked} onChange={changeNewProductStocked}/>
      </label>
      <br/>
      <label>
        Name:
        <input type="text" value={newProduct.name} onChange={changeNewProductName}/>
      </label>
      <br/>
      <input type="submit" value="Submit"/>
    </form>
  );
};

export default AddProduct;
