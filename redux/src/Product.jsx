import React from 'react';

import ActionLink from './ActionLink';

const Product = props => {
  const removeProduct = () =>
    props.store.dispatch({
      type: "REMOVE_PRODUCT",
      productName: props.name
    });

  const productName = props.stocked
    ? props.name
    : <font color="red">{props.name}</font>;

  return (
    <tr>
      <td>{productName}</td>
      <td>{props.price}</td>
      <td>
        <ActionLink text="Remove" onClick={removeProduct}/>
      </td>
    </tr>
  );
};

export default Product;
