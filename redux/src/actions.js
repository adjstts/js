/* Action types */

export const CHANGE_SEARCH_STRING = "CHANGE_SEARCH_STRING";

export const TOGGLE_ONLY_SHOW_PRODUCTS_IN_STOCK = "TOGGLE_ONLY_SHOW_PRODUCTS_IN_STOCK";

export const REMOVE_PRODUCT = "REMOVE_PRODUCT";

export const CHANGE_NEW_PRODUCT_CATEGORY = "CHANGE_NEW_PRODUCT_CATEGORY";
export const CHANGE_NEW_PRODUCT_PRICE = "CHANGE_NEW_PRODUCT_PRICE";
export const CHANGE_NEW_PRODUCT_STOCKED = "CHANGE_NEW_PRODUCT_STOCKED";
export const CHANGE_NEW_PRODUCT_NAME = "CHANGE_NEW_PRODUCT_NAME";

export const ADD_PRODUCT = "ADD_PRODUCT";


/* Action creators */

export const changeSearchString = value => ({
  type: CHANGE_SEARCH_STRING,
  value // string
});

export const toggleOnlyShowProductsInStock = value => ({
  type: TOGGLE_ONLY_SHOW_PRODUCTS_IN_STOCK,
  value // boolean
});

export const removeProduct = productName => ({
  type: REMOVE_PRODUCT,
  productName // string
});

export const changeNewProductCategory = newProductCategory => ({
  type: CHANGE_NEW_PRODUCT_CATEGORY,
  newProductCategory // string
});

export const changeNewProductPrice = newProductPrice => ({
  type: CHANGE_NEW_PRODUCT_PRICE,
  newProductPrice // string
});

export const changeNewProductStocked = newProductStocked => ({
  type: CHANGE_NEW_PRODUCT_STOCKED,
  newProductStocked // boolean
});

export const changeNewProductName = newProductName => ({
  type: CHANGE_NEW_PRODUCT_NAME,
  newProductName // string
});

export const addProduct = (category, price, stocked, name) => ({
  type: ADD_PRODUCT,
  category,
  price,
  stocked,
  name
});
